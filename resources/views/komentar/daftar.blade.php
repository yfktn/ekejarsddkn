@if($komentar->count()>0)
    @foreach($komentar as $k)
    <div class="item">
        <span class="time pull-right"><i class="fa fa-calendar"></i> {{ $k->created_at->format('d-m-Y H:m') }}</span>
        <div class="timeline-comment-user"><i class="fa fa-user"></i> {{ $k->user->name }}</div>
        <div class="timeline-comment-content">
            {!! nl2br($k->isi) !!}
        </div>
    </div>
    @endforeach
@else
    <div class="item">
        <div class="timeline-comment-content">Belum ada komentar tercatat.</div>
    </div>
@endif