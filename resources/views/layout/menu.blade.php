<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        {{--<div class="user-panel">--}}
            {{--<div class="pull-left image">--}}
                {{--<img src="{{ asset('lte2.3/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">--}}
            {{--</div>--}}
            {{--<div class="pull-left info">--}}
                {{--<p>{{ Auth::user()->name }}</p>--}}
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-ic-indicator="#indic-loader">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="active">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            @if(Authorization::getIsLoggedUserRoleAs('admin'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Master</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> SKPD</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> User</a></li>
                </ul>
            </li>
            @endif
            <li>
                <a data-ic-get-from="{{ route('draft') }}" data-ic-target="#content-load" data-ic-push-url="true">
                    <i class="fa fa-anchor"></i> <span>Permintaan Data SDDKN</span>
                </a>
            </li>
            <li>
                <a data-ic-get-from="{{ route('mengingat') }}" data-ic-target="#content-load" data-ic-push-url="true">
                    <i class="fa fa-life-bouy"></i> <span>Tata Cara Penggunaan</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

