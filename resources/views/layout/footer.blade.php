<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>ekejarSDDKN 0.1</b>
        <b style="display: none;">Version 0.1</b>
    </div>
    <strong>IT KALTENG</strong>
    <strong style="display: none;">Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>