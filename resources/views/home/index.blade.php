@extends($layout)
@section('content-header')
    <h1>Kejar SDDKN - {{ \Session::get('CurrUserSKPDName') }}</h1>
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-bullhorn"></i>
            <h3 class="box-title">Perhatian</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="callout callout-danger">
                <h4>Perhatian</h4>
                <p>Lagi prototype!</p>
            </div>
        </div><!-- /.box-body -->
    </div>
@endsection