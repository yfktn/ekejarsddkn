@extends($layout)
@section('content-header')
    <h1>Tool Pembantuan<small>standarisasi isi mengingat</small></h1>
@endsection
@section('content')
<section id="tool-mengingat">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="keywords" name="keywords" class="form-control"
                                   placeholder="Masukkan kunci pencarian" type="text">
                            <a ic-post-to="{{ route('mengingat.postPencarian') }}"
                               ic-include="#keywords" ic-target="#hasil-cari"
                               class="btn btn-warning input-group-addon">Cari</a>
                        </div>
                        <div class="help-block">Tambahkan kutip dua untuk mencari urutan kata yang detail.</div>
                    </div>
                </div>
            </div>
            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">Hasil Pencarian</h3>
                </div>
                <div class="box-body" id="hasil-cari">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Item Pengingat Dipilih</h3>
                </div>
                <div class="box-body">
                    <ol id="pengingat"></ol>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('late-script')
<script type="text/javascript">
function tambahPengingat(e,o) {
    e.preventDefault();
    var t = $(o).children('.list-group-item-text:first-child').text();
    $('ol#pengingat').append('<li><span>'+t+'</span></li>');
}
</script>
@endsection