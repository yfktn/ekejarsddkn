{{--Loading hasil pencarian yang didapatkan dari katakunci--}}
<div class="list-group">
    @if((is_object($result) && $result->count()<=0) || $result == null)
        <div class="list-group-item list-group-item-danger">
            <span class="list-group-item-text">Tidak ada data</span>
        </div>
    @else
        @foreach($result as $r)
        <a href="#" onclick="tambahPengingat(event,this);" class="list-group-item">
            <span class="list-group-item-text">{{ $r->catatan_lengkap }}</span>
        </a>
        @endforeach
    @endif
</div>