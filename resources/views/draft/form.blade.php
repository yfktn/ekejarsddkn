<form id="frmProfile" class="form-horizontal" role="form"
    {{ $method=='post'?"ic-post-to":"ic-put-to" }}="{{ $action }}" ic-indicator="#js-indic">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="{{ $method }}">
    <div id="alerter-success" class="form-group" style="display: none;">
        <div class="col-md-12">
            <div class="alert alert-success">
                <span id="message-success"></span>
            </div>
        </div>
    </div>
    <div id="alerter-error" class="form-group" style="display: none;">
        <div class="col-md-12">
            <div class="alert alert-error">
                <span id="message-error"></span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="judul" class="col-md-3 control-label">Judul Draft Peraturan</label>
        <div class="col-md-9">
            <input type="text" id="judul" class="form-control" name="judul"
                   value="{{ load_input_value($data, "judul") }}" maxlength="255">
            <div id="error-judul" class="error"></div>
        </div>
    </div>
    <div class="form-group">
        <label for="judul" class="col-md-3 control-label">Jenis Aturan</label>
        <div class="col-md-9">
            {!! load_select_model('jenis_aturan', \Eh\Factories\JenisAturanFactory::lists(), $data,
                ['class'=>'form-control'], ['Pilih Jenis Aturan']) !!}
            <div id="error-jenis_aturan" class="error"></div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="btn btn-primary">
                <i id="js-indic" class="fa fa-spin fa-spinner" style="display: none"></i> Simpan
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
function resetM()
{
    TSSTMIK.resetFormErrorMsg('form#frmProfile div.error');
    $('#alerter-error, #alerter-success').hide();
}
$('#frmProfile').on('error.ic', function (evt, elt, stat, str, xhr) {
    resetM();
    if(xhr.status==422) {
        TSSTMIK.showFormErrorMsg(xhr.responseText);
    } else {
        $('#message-error').text(str).closest('div.form-group').show();
    }
})
</script>