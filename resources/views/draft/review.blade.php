{{--
Lakukan proses untuk melakukan render review disini
Gunakan timeline milik LTE
--}}
@extends($layout)
@section('content-header')
    <h1>Review Proses<small>pengajuan draft</small></h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-anchor"></i> <a ic-get-from="{{ route('draft') }}" ic-target="#content-load" ic-push-url="true">Daftar Pengajuan</a></li>
        <li class="active">Review</li>
    </ol>
@endsection
@section('content')
<section id="review-section">
    <div class="row">
        <div class="col-md-12">
            <?php $currDate = $draft->created_at->format('d-m-Y'); ?>
            {{--Tampilkan lebih dulu bagian atas untuk apa yang tampil di draft awal--}}
            <ul class="timeline">
                <li class="time-label">
                    <span class="bg-maroon">{{ $currDate }}</span>
                </li>
                <li>
                    <i class="fa fa-bell-o bg-red"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-user"></i> {{ $draft->user->name }}  <i class="fa fa-clock-o"></i> {{ $draft->created_at->format('H:m') }}</span>
                        <h3 class="timeline-header">Draft Diajukan</h3>
                        <div class="timeline-body">
                            <dl class="dl-horizontal">
                                <dt>Judul</dt>
                                <dd>{{ $draft->judul }}</dd>
                                <dt>Pengaju</dt>
                                <dd>{{ $draft->user->name or "unknown" }}</dd>
                                <dt>Status</dt>
                                <dd>{{ \Eh\Factories\DraftFactory::getStatusLists($draft->status) }}</dd>
                                <dt>Tipe Proses</dt>
                                <dd>{{ \Eh\Factories\DraftFactory::getTypeProsesLists($draft->proses_type) }}</dd>
                                <dt>Jenis Aturan</dt>
                                <dd>{{ \Eh\Factories\JenisAturanFactory::lists($draft->jenis_aturan) }}</dd>
                            </dl>
                        </div>
                    </div>
                </li>
                {{--Sekarang saatnya membuat untuk fileDraft--}}
                <?php $cnt = 0; ?>
                @foreach($filesDraft as $fd)
                    <?php $cnt += 1; ?>
                    <?php $ddt = $fd->created_at->format('d-m-Y'); ?>
                    @if($currDate!=$ddt)
                        <?php $currDate = $ddt; ?>
                        <li class="time-label">
                            <span class="bg-fuchsia">{{ $currDate }}</span>
                        </li>
                    @endif
                    <li>
                        @if($fd->sumber==\Eh\FileDraft::SUMBER_PENGAJU)
                            <i class="fa fa-user-secret {{ $cnt ==1 ? "bg-orange": "bg-red" }}"></i>
                        @else
                            <i class="fa fa-street-view bg-black"></i>
                        @endif
                        <div class="timeline-item">
                            <span class="time">
                                <i class="fa fa-user"></i> {{ $fd->user->name }}
                                <i class="fa fa-clock-o"></i> {{ $fd->created_at->format('H:m') }}
                            </span>
                            <h3 class="timeline-header">
                            @if($cnt==1)
                                File Draft Awal Diajukan
                            @else
                                @if($fd->sumber==\Eh\FileDraft::SUMBER_PENGAJU)
                                    File Revisi Draft Diajukan
                                @else
                                    File Supervisi Draft
                                @endif
                            @endif
                            </h3>
                            <div class="timeline-body">
                                {{ $fd->keterangan }}
                                <hr class="separator bg-orange">
                                <ul>
                                    @foreach($fd->attachments as $f)
                                    <li><a target="_blank" href="{{ $f->targetURLPath('/uploads/') }}">{{ $f->client_file_name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div id="komentarnya-{{$fd->id}}" class="timeline-comments bg-orange">
                                @include('komentar.daftar', ['komentar'=>$fd->comments])
                            </div>
                            <div class="timeline-footer">
                                <form id="f{{$fd->id}}" class="js-frm-komentar" ic-post-to="{{ route('draft.postKomentarDiFile',
                                    ['fileDraftId'=>$fd->id, 'targetOwnerId'=>"komentarnya-{$fd->id}"]) }}"
                                        ic-target="#komentarnya-{{$fd->id}}">
                                <textarea class="form-control" name="isi" id="isi"
                                          placeholder="Ketikkan Komentar Disini"></textarea>
                                <button type="submit" class="btn btn-xs bg-orange"><i class="fa fa-comment"></i> Kirim Komentar</button>
                                </form>
                            </div>
                        </div>
                    </li>
                @endforeach
                {{--terakhir--}}
                <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                    <div class="timeline-item">
                        {{--
                        lakukan proses untuk mengatur tampilan tombol berdasarkan role yang dimiliki oleh yang login
                        --}}
                        @if($draft->status==\Eh\Draft::STATUS_PROSES)
                            <?php $cntFileDrafts = $draft->fileDrafts()->count(); ?>
                            @if(Authorization::getIsLoggedUserRoleAs('operator'))
                                {{--Hanya user yang membuat yang bisa melakukan editing dan menghapus--}}
                                @if($cntFileDrafts <= 1 && $draft->user_id == Auth::user()->id)
                                    {{--TODO: Buat fasilitas editing Draft--}}
                                    {{--<a href="#" class="btn bg-blue btn-sm"><i class="fa fa-edit"></i> Edit</a>--}}
                                    <a ic-delete-from="{{ route('draft.delete', ['id'=>$draft->id]) }}" ic-target="closest ul.timeline"
                                       ic-confirm="Yakin untuk menghapus data?"
                                       class="btn bg-red btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                @endif
                                @if($cntFileDrafts<=0)
                                    {{--Apakah dimunculkan upload draft awal?--}}
                                    <a href="{{ route('draft.getUploadDraftAwal', ['id'=>$draft->id, 'targetOwnerId'=>'review-section']) }}"
                                       class="btn bg-maroon btn-sm js-modal-draft-awal" data-target="#file-draft-modal">
                                        <i class="fa fa-file"></i> Upload Draft Awal</a>
                                @else
                                    {{--atau dimunculkan draft revisi?--}}
                                    <a href="{{ route('draft.getUploadDraftRevisi', ['id'=>$draft->id, 'targetOwnerId'=>'review-section']) }}"
                                       class="btn bg-maroon btn-sm js-modal-draft-revisi" data-target="#file-draft-modal">
                                        Upload File Draft Revisi
                                    </a>
                                @endif
                            @elseif(Authorization::getIsLoggedUserRoleAs('supervisor'))
                                @if($cntFileDrafts>0)
                                    <a href="{{ route('draft.getUploadDraftRevisi', ['id'=>$draft->id, 'targetOwnerId'=>'review-section']) }}"
                                       class="btn bg-maroon btn-sm js-modal-draft-revisi" data-target="#file-draft-modal">
                                        Upload File Draft Supervisi
                                    </a>
                                @else
                                    <p><span class="label label-info">Hi!</span> Menunggu Draft Awal Untuk Di Upload!</p>
                                @endif
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
                                <a ic-post-to="{{ route('draft.postSetStatusDraft',
                                    ['id'=>$draft->id, 'status'=>\Eh\Draft::STATUS_FINAL, 'targetOwnerId'=>'review-section']) }}"
                                   ic-include="#csrf_token" ic-confirm="Apakah yakin untuk DRAFT status menjadi FINAL?"
                                   class="btn bg-blue btn-sm js-button-setstatus" data-target="#file-draft-modal">
                                    <i class="fa fa-check"></i> Set Status Draft Sebagai FINAL
                                </a>
                                <a ic-post-to="{{ route('draft.postSetStatusDraft',
                                    ['id'=>$draft->id, 'status'=>\Eh\Draft::STATUS_DIBATALKAN, 'targetOwnerId'=>'review-section']) }}"
                                   ic-include="#csrf_token" ic-confirm="Draft akan ditandai sebagai DIBATALKAN, apakah yakin?"
                                   class="btn bg-red btn-sm js-button-setstatus" data-target="#file-draft-modal">
                                    <i class="fa fa-close"></i> Set Status Draft DIBATALKAN
                                </a>
                            @endif
                        @else
                            <div class="timeline-body">
                                <span class="label label-info">Hi!</span>Status Draft Telah Di Set Sebagai
                                <b>{{ \Eh\Factories\DraftFactory::getStatusLists($draft->status) }}</b>
                            </div>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {{--
    Disini kita memberikan akses untuk modal dialog yang akan digunakan untuk beberapa kebutuhan,
    seperti upload file
    --}}
    <div class="modal fade"
         id="file-draft-modal"
         tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Draft Dialog</h4>
                </div>
                <div id="modal-body" class="modal-body">Tunggu Sedang loading ...</div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('late-script')
<script type="text/javascript">
    $('a.js-button-setstatus').on('success.ic',function(evt, elt, data, textStatus, xhr, requestId){
        window.location.reload(true); // reload saja!
    }).on('error.ic', function (evt, elt, stat, str, xhr) {
        if(xhr.status==422) {
            alert('Error: kesalahan terhadap inputan');
        }else {
            alert('Ada kesalahan di server! Hubungi pengembang');
        }
    });
    $('form.js-frm-komentar').on('success.ic',function(evt, elt, data, textStatus, xhr, requestId){
        $(this).children('textarea').val("");
    }).on('error.ic', function (evt, elt, stat, str, xhr) {
        if(xhr.status==422) {
            alert('Error:Coba check inputan komentar yang harus diisi semua');
        }else {
            alert('Ada kesalahan di server!');
        }
    });
    $('a.js-modal-draft-awal').on('click', function(e) {
        e.preventDefault();
        idM = $(this).data('target');
        $(idM + ' h4.modal-title').text('Upload Draft Awal');
        $(idM + ' div#modal-body').text('in Loading').load($(this).attr('href'), function(result){
            $(idM).modal({show:true});
        });
    });
    $('a.js-modal-draft-revisi').on('click', function(e) {
        e.preventDefault();
        idM = $(this).data('target');
        $(idM + ' h4.modal-title').text('Upload Draft Revisi');
        $(idM + ' div#modal-body').text('in Loading').load($(this).attr('href'), function(result){
            $(idM).modal({show:true});
        });
    });
//    Intercooler.processNodes($('section#review-section'));
</script>
@endsection
