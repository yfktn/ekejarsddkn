{{--
Digunakan untuk menampilkan daftar Draft yang di load pada halaman index.
$data adalah collection
--}}
@if(0==$data->count())
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-bullhorn"></i>
            <h3 class="box-title">Perhatian</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="callout callout-danger">
                <h4>Belum Ada Data</h4>
                <p>Hingga saat ini belum ada draft peraturan diajukan.</p>
            </div>
        </div><!-- /.box-body -->
    </div>
@else
    @foreach($data as $d)
        @if($d->status == \Eh\Draft::STATUS_PROSES)
        <div class="box box-info box-solid">
        @elseif($d->status == \Eh\Draft::STATUS_FINAL)
        <div class="box box-success box-solid">
        @elseif($d->status == \Eh\Draft::STATUS_DIBATALKAN)
        <div class="box box-danger box-solid">
        @else
        <div class="box box-warning box-solid">
        @endif
            <div class="box-header with-border">
                {{--TODO: tambahkan ikon yang bisa membedakan berdasarkan status!--}}
                <h3 class="box-title">Draft {{ \Illuminate\Support\Str::limit($d->judul, 50) }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>Judul</dt>
                    <dd>{{ $d->judul }}</dd>
                    <dt>Pengaju</dt>
                    <dd>{{ $d->user->name or "unknown" }}</dd>
                    <dt>Status</dt>
                    @if(0==$d->fileDrafts()->count())
                        <dd class="text-red">Membutuhkan Draft Awal Untuk Di Upload</dd>
                    @else
                        @if($d->status == \Eh\Draft::STATUS_PROSES)
                        <dd class="text-green">
                        @elseif($d->status == \Eh\Draft::STATUS_FINAL)
                        <dd class="text-maroon">
                        @elseif($d->status == \Eh\Draft::STATUS_DIBATALKAN)
                        <dd class="text-red">
                        @else
                        <dd class="text-warning">
                        @endif
                            {{ \Eh\Factories\DraftFactory::getStatusLists($d->status) }}
                        </dd>
                    @endif
                    <dt>Jenis Aturan</dt>
                    <dd>{{ \Eh\Factories\JenisAturanFactory::lists($d->jenis_aturan) }}</dd>
                    <dt>Diajukan Sejak</dt>
                    <dd>{{ $d->created_at->format('d-m-Y') }}</dd>
                    <dt>SKPD Pengaju</dt>
                    <dd>{{ $d->skpd->nama }}</dd>
                </dl>
                <span class="pull-right">
                    {{--Hanya user yang membuat yang bisa melakukan editing dan menghapus--}}
                    @if($d->fileDrafts()->count() <= 1 && $d->user_id == Auth::user()->id)
                        <a href="#" class="btn bg-blue btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        <a ic-delete-from="{{ route('draft.delete', ['id'=>$d->id]) }}" ic-target="closest div.box"
                           ic-confirm="Yakin untuk menghapus data?"
                           class="btn bg-red btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                    @endif
                    {{--
                    TODO: Butuh sebuah proses optimasi disini supaya tidak selalu melakukan request!
                    Tampilkan tempat upload draft awal bila tidak ada data draft dimasukkan lebih dahulu!
                    --}}
                    @if(0==$d->fileDrafts()->count())
                        @if(Authorization::getIsLoggedUserRoleAs('operator'))
                        {{--Untuk aktifasi tampilnya modal ada pada script index, supaya bisa diload pada bagian akhir!--}}
                        <a href="{{ route('draft.getUploadDraftAwal', ['id'=>$d->id, 'targetOwnerId'=>'draft-modal']) }}"
                           class="btn bg-maroon btn-sm js-modal-draft-awal" data-target="#draft-modal">
                            <i class="fa fa-file"></i> Upload Draft Awal</a>
                        @endif
                    @else
                        <a ic-get-from="{{ route('draft.getReview',['id'=>$d->id]) }}" ic-target="#content-load" ic-push-url="true"
                           class="btn bg-yellow btn-sm">Review Proses</a>
                    @endif
                </span>
            </div><!-- /.box-body -->
        </div>
    @endforeach
@endif