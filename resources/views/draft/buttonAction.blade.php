{{-- Tampilkan untuk tombol aksi dibagian atas index --}}
<div class="btn-group pull-right">
    <a ic-get-from="{{ route('draft.getCreateForm') }}" ic-target="div#js-draft-act" class="btn btn-info">
        <i class="fa fa-plus"></i> Ajukan Draft Baru</a>
</div>