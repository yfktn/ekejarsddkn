@extends($layout)
@section('content-header')
    <h1>Draft Peraturan<small>diajukan dan status</small></h1>
@endsection
@section('content')
<section id="draft-list-section">
    {{--Yakinkan bahwa role adalah operator--}}
    @if(Authorization::getIsLoggedUserRoleAs('operator'))
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body" id="js-draft-act">
                    @include('draft.buttonAction')
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div id="draft-list" ic-src="{{ route('draft.getDraftList') }}" ic-trigger-on="ic-draft-list-trigger" class="col-md-12">
            @include('draft.draftList')
        </div>
    </div>
    {{--
    Disini kita memberikan akses untuk modal dialog yang akan digunakan untuk beberapa kebutuhan,
    seperti upload file
    --}}
    <div class="modal fade"
         id="draft-modal"
         tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Draft Dialog</h4>
                </div>
                <div id="modal-body" class="modal-body">Tunggu Sedang loading ...</div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('late-script')
<script type="text/javascript">
    $('#draft-list-section').on('draft/icCreateFormSuccess', function()
    {
        $('div#draft-list').trigger('ic-draft-list-trigger');
    }).on('draft/icDeleteDraft', function()
    {
        $('div#draft-list').trigger('ic-draft-list-trigger');
    });
    $('a.js-modal-draft-awal').on('click', function(e) {
        e.preventDefault();
        idM = $(this).data('target');
        $(idM + ' div#modal-body').text('in Loading').load($(this).attr('href'), function(result){
            $(idM).modal({show:true});
        });
    })
</script>
@endsection