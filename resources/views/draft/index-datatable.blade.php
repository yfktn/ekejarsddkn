@extends($layout)
@section('content-header')
    <h1>Draft Peraturan<small>diajukan dan status</small></h1>
@endsection
@section('content')
    <section id="draft-list-section">
        {{--Yakinkan bahwa role adalah operator--}}
        @if(Authorization::getIsLoggedUserRoleAs('operator'))
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-body" id="js-draft-act">
                            @include('draft.buttonAction')
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    {{--<div class="box-header with-border">--}}
                        {{--<i class="fa fa-bullhorn"></i>--}}
                        {{--<h3 class="box-title">Data Destinasi Wisata</h3>--}}
                    {{--</div>--}}
                    <!-- /.box-header -->
                    <div class="box-body" data-ic-target="#content-load">
                        <table id="data-table" class="table"
                                {{--data-toolbar="#dt-toolbar"--}}
                               data-url="{{ route('draft.getDataBTTable') }}"
                               data-pagination="true"
                               data-classes="table table-hover table-condensed"
                               data-striped="true"
                               data-side-pagination="server"
                               data-page-list="[5, 10, 20, 50, 100, 200]"
                               data-search="true"
                               data-show-toggle="true"
                                {{--data-query-params="addFilterValue"--}}
                               data-mobile-responsive="true">
                            <thead>
                            <tr>
                                <th data-field="id" data-visible="false">ID</th>
                                <th data-field="nama">SKPD</th>
                                <th data-field="judul" data-sortable="true">Judul</th>
                                {{--<th data-field="jenis_aturan" data-sortable="true">Jenis Aturan</th>--}}
                                <th data-field="status" data-sortable="true">Status</th>
                                <th data-field="proses_type" data-sortable="true">Tipe Proses</th>
                                <th data-width="100px" data-formatter="loadAksi" data-events="eventAksi">Aksi</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--
        Disini kita memberikan akses untuk modal dialog yang akan digunakan untuk beberapa kebutuhan,
        seperti upload file
        --}}
        <div class="modal fade"
             id="draft-modal"
             tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Draft Dialog</h4>
                    </div>
                    <div id="modal-body" class="modal-body">Tunggu Sedang loading ...</div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('late-script')
<script type="text/javascript">
    function LoadBT() {
        $('#data-table').bootstrapTable();
    }
    $(document).ready(function () {
        TSSTMIK.loadBootstrapTableScript(LoadBT);
    });
    function loadAksi(value, row, index) {
        btns = [
            '<a class="btn btn-sm bg-red js-review" data-ic-push-url="true" data-ic-indicator="#indic-loader" data-ic-src="/draft/getReview/' + row['id'] +'" title="Process Review">&nbsp;<i class="fa fa-binoculars"></i> Review</a>',
//            '<a class="btn btn-sm btn-danger js-delete" data-ic-confirm="Yakin untuk menghapus?" data-ic-target="closest tr" data-ic-delete-from="/draft/delete/' + row['id'] +'" title="Delete">&nbsp;<i class="fa fa-trash-o"></i> </a>'
        ];
        return btns.join('&nbsp;');
    }
    $('#draft-list-section').on('draft/icCreateFormSuccess', function()
    {
        $('#data-table').bootstrapTable('refresh');
    });
    $('a.js-modal-draft-awal').on('click', function(e) {
        e.preventDefault();
        idM = $(this).data('target');
        $(idM + ' div#modal-body').text('in Loading').load($(this).attr('href'), function(result){
            $(idM).modal({show:true});
        });
    });
    window.eventAksi = {
        'click .js-review': function (e, value, row, index) {
            Intercooler.triggerRequest($(this));
        }
    };
</script>
@endsection