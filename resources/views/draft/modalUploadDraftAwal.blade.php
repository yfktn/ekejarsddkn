{{-- render tampilan untuk di load di dialog modal saat menambahkan file untuk draft awal
 Ini digunakan pula oleh proses upload draft revisi!
--}}
<iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;display:none;"></iframe>
<p id="upload_progress" style="display: none;"><i class="fa fa-spinner fa-spin"></i> Uploading In progress ... Please wait!</p>
<form id="formupload" role="form" action="{{ (isset($routes)?$routes:$routesUpload) }}" enctype="multipart/form-data" method="post"
      target="upload_target">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <textarea name="keterangan" id="keterangan" class="form-control"
                  placeholder="Isikan dengan keterangan mengenai file di upload"></textarea>
    </div>
    <div class="form-group">
        <label for="filea">Upload File Draft</label>
        <input type="file" name="filea[]" id="filea" class="form-control" placeholder="Pilih File Attachment" multiple>
        <div class="help-block">Silahkan pilih file lebih dari satu dengan memilih File sembari menekan tombol CTRL</div>
        <div id="error-filea" class="error"></div>
    </div>
    <div class="form-group">
        <input name="act" type="submit" class="btn btn-info" value="Upload">
    </div>
</form>
<script type="text/javascript">
toID = "#{{ $targetOwnerId }}";
$(toID).on('startUpload', function(e) {
    $('#upload_progress').toggle();
    $('#formupload').toggle();
}).on('afterUpload', function(e,filecount, fileuploaded) {
    $('#upload_progress').toggle();
    $('#formupload')
            .toggle()
            .trigger('reset');
    if(fileuploaded>0) {
        alert('Telah terupload '+fileuploaded+' file dari '+filecount+' file terpilih. Browser akan di refresh ...');
        window.location.reload(true); // reload saja!
    } else {
        alert('Terjadi kesalahan, tidak ada file yang terupload!');
    }
}).on('validateError', function(e,msg){
    alert('Validator Error: '+msg);
    $('#upload_progress').toggle();
    $('#formupload')
            .toggle();
});
$('form#formupload').on('submit', function(e){
    $(toID).trigger('startUpload', [e]);
});
</script>