<?php

namespace Eh;

use Illuminate\Database\Eloquent\Model;

class FileDraft extends Model
{
    const SUMBER_PENGAJU = 'pengaju';
    const SUMBER_SUPERVISOR = 'supervisor';

    protected $table = 'file_draft';
    protected $fillable = ['keterangan'];
    protected $touches = ['draft'];

    /**
     * Relasi ke Draft
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function draft()
    {
        return $this->belongsTo('Eh\Draft');
    }
    /**
     * Link polymorphic ke attachments
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany('Eh\FileAttachment', 'owner');
    }

    public function comments()
    {
        return $this->morphMany('Eh\Komentar', 'owner');
    }

    public function user()
    {
        return $this->belongsTo('Eh\User');
    }
}
