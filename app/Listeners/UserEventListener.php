<?php

namespace Eh\Listeners;

class UserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handleOnLogin($user, $remember)
    {
        $skpd = $user->skpd;
        if(!is_null($skpd))
        {
            \Session::set('CurrUserSKPD', $skpd->id);
            \Session::set('CurrUserSKPDName', $skpd->nama);
        }
        else
        {
            \Session::set('CurrUserSKPD', 0);
            \Session::set('CurrUserSKPDName', 'Biro Administrasi SETDA Prov Kalteng');
        }
    }

    public function handleOnAttempt($payload)
    {

    }

    public function handleOnLogout($user)
    {
        \Session::set('CurrUserSKPD', 0);
        \Session::set('CurrUserSKPDName', '');
    }

    public function subscribe($events)
    {
        $events->listen('auth.login', '\Eh\Listeners\UserEventListener@handleOnLogin');
        $events->listen('auth.attempt', '\Eh\Listeners\UserEventListener@handleOnAttempt');
        $events->listen('auth.logout', '\Eh\Listeners\UserEventListener@handleOnLogout');
    }
}
