<?php

namespace Eh;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class SKPD extends Model
{
    use NodeTrait;

    protected $table = 'skpd';
    protected $fillable = ['nama'];
}
