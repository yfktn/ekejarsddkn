<?php
/**
 * Factory untuk Draft!
 * User: toni
 * Date: 16/11/15
 * Time: 21:22
 */

namespace Eh\Factories;


use Eh\Draft;
use Eh\FileDraft;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Panatau\Tools\UtilBootstrapTable;

class DraftFactory extends AbstractFactory
{

    /**
     * Kembalikan daftar status yang bisa pula digunakan sebagai bahan untuk di load di combo box
     * @param null $d bila null kembalikan array bila tidak kembalikan nilai label nya
     * @return array
     */
    public static function getStatusLists($d = 0)
    {
        $ar = [
            Draft::STATUS_PROSES => 'Request Sedang Diproses',
            Draft::STATUS_FINAL => 'Request Berstatus Final',
            Draft::STATUS_DIBATALKAN => 'Request Dibatalkan',
            Draft::STATUS_DITUTUP => 'Request Ditutup'
        ];
        return 0 === $d ?
            $ar:
            $ar[$d];
    }

    /**
     * Kembalikan nilai tipe proses status
     * @param null $d
     * @return array
     */
    public static function getTypeProsesLists($d = null)
    {
        $ar = [
            Draft::PROSES_TYPE_TA => 'Menunggu Draft Awal',
            Draft::PROSES_TYPE_MP => 'Menunggu Revisi',
            Draft::PROSES_TYPE_MS => 'Menunggu Supervisi',
            Draft::PROSES_TYPE_FS => 'Proses Selesai',
        ];
        return null === $d ?
            $ar:
            $ar[$d];
    }

    /**
     * Menyimpan data baru!
     * @param array $input
     * @return bool
     */
    public function store(array $input)
    {
        return $this->realSave($input, new Draft());
    }

    /**
     * Generalisasi untuk penyimpanan
     * @param array $input
     * @param Draft $model
     * @return bool
     */
    protected function realSave(array $input, Draft $model)
    {
        $editmode = true === $model->exists;
        $model->fill($input);
        if(!$editmode)
        {
            $model->user_id = \Auth::user()->id;
            $model->skpd_id = \Session::get('CurrUserSKPD');
            $model->status = Draft::STATUS_PROSES; // set status proses
            $model->proses_type = Draft::PROSES_TYPE_TA; // set proses tipe menunggu draft Awal untuk ini!
        }
        return $model->save();
    }

    /**
     * Dapatkan untuk data index
     * TODO: bagaimana kalau per halaman?
     * @return mixed
     */
    public function getDataIndex()
    {
        // check berdasarkan skpd_id
        if(\Session::get('CurrUserSKPD')!==0)
        {
            return Draft::where('skpd_id', '=',\Session::get('CurrUserSKPD'))->orderBy('created_at', 'desc')->get();
        }
        return Draft::orderBy('created_at', 'desc')->get();
    }

    /**
     * Dapatkan draft!
     * @param $id
     * @return mixed
     */
    public function getDraft($id)
    {
        return Draft::findOrFail($id);
    }

    /**
     * @param Draft $draft
     * @return Collection of the fileDraft
     */
    public function getFilesDraft(Draft $draft)
    {
        return $draft->fileDrafts;
    }

    /**
     * Set status draft!
     * @param Draft $draft
     * @param $status
     */
    public function setStatusDraft(Draft $draft, $status)
    {
        $draft->status = $status;
        $draft->proses_type = Draft::PROSES_TYPE_FS; // set sebagai finish!
        return $draft->save();
    }

    /**
     * Hapus draft beserta fileDraft bila ada!
     * @param Draft $draft
     */
    public function delete(Draft $draft)
    {
        try
        {
            \DB::transaction(function() use(&$draft) {
                if($draft->fileDrafts()->count()>1)
                {
                    $this->errors->add('system', 'Draft sudah memiliki revisi, tidak dapat dihapus!');
                    throw new \Exception('Draft sudah memiliki revisi, tidak dapat dihapus!');
                }
                // hapus dulu file bila ada
                // TODO: perlu di optimasi dan juga error trapping perlu dipikirkan.
                $fd = $draft->fileDrafts; // ambil record fileDrafts
                foreach ($fd as $f) {
                    // ambil lagi fileAttachments!
                    foreach ($f->attachments as $fl) {
                        $fl->deleteFile(public_path('uploads'));
                    }
                    $f->attachments()->delete();
                }
                // hapus filedraft!
                $draft->fileDrafts()->delete();
                $draft->delete();
            });
        }
        catch(\Exception $e)
        {
            \Log::emergency('Gagal menghapus Draft::'.$e->getMessage()."\n".$e->getTraceAsString(), ["DraftFactory::delete"]);
            if($this->errors->count()<=0) $this->errors->add('system', $e->getMessage());
        }
        return $this->errors->count()<=0;
    }

    public function getBTTable($pagination)
    {
        $draft = \DB::table('draft')
            ->join('skpd', 'skpd.id', '=', 'draft.skpd_id');
        // check berdasarkan skpd_id
        if(\Session::get('CurrUserSKPD')!==0)
        {
            $draft = $draft->where('draft.skpd_id', '=',\Session::get('CurrUserSKPD'));
        }
        $bttable = new UtilBootstrapTable(
            $draft = $draft->orderBy('draft.created_at', 'desc')
        );
        $draft = $draft->select(['draft.id', 'draft.judul', 'draft.status', 'draft.jenis_aturan', 'skpd.nama', 'draft.proses_type']);

        $data = $bttable
            ->search(['judul', 'status', 'proses_type', 'nama'], $pagination['search'])
            ->setPaginationOffset($pagination['offset'])
            ->setPaginationLimit($pagination['limit'])
            ->setSortBy($pagination['sort'], $pagination['order'])
            ->getData();

        return '{'
        .'"total": '. $bttable->getDbCount() .','
        .'"rows": ' . json_encode($data)
        .'}';
    }
}