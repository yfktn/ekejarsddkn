<?php
/**
 * Disini tidak dipakai!
 * Kembalikan nilai jenis aturan
 * User: toni
 * Date: 16/11/15
 * Time: 20:50
 */

namespace Eh\Factories;


class JenisAturanFactory extends AbstractFactory
{
    /**
     * Kembalikan nilai lists untuk daftar jenis aturan yang bisa digunakan untuk dropdown
     * @return array
     */
    public static function lists($d=null)
    {
        $ar = [
            'perda' => 'Peraturan Daerah',
            'pergub' => 'Keputusan Gubernur'
        ];
        return null === $d ?
            $ar:
            $ar[$d];
    }
}