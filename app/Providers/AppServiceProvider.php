<?php

namespace Eh\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // tambah blade
        // @runX($a=$b)
        \Blade::directive('runX', function($expression){
            return "<?php {$expression}; ?>";
        });
        // only load when we'are in local env
        if($this->app->environment()=='local')
        {
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
        }
        // register disini untuk mengurangi polusi
        $this->app->register('Panatau\Tools\PanatauToolsServiceProvider');
        $this->app->register('Panatau\Authorization\PanatauAuthorizationServiceProvider');
        $this->app->register('Panatau\Todos\TodosServiceProvider');
    }
}
