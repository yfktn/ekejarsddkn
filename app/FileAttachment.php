<?php

namespace Eh;

use Illuminate\Database\Eloquent\Model;
use Panatau\Tools\FileAttachmentModelTrait;

class FileAttachment extends Model
{
    use FileAttachmentModelTrait;
    protected $table = 'file_attachment';
    protected $guarded = ['id'];
    public $timestamps = false;

    /**
     * ini adalah polymorphisme
     */
    public function owner() {
        return $this->morphTo();
    }
}
