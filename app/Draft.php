<?php

namespace Eh;

use Illuminate\Database\Eloquent\Model;

class Draft extends Model
{
    const STATUS_PROSES = 'proses';
    const STATUS_FINAL = 'final';
    const STATUS_DIBATALKAN = 'dibatalkan';
    const STATUS_DITUTUP = 'ditutup';

    /**
     * Disini apabila draft sudah ditambahkan maka saat user menambahkan revisi status akan menjadi menunggu supervisi,
     * apabila supervisi di tambahkan maka status menjadi menunggu revisi, hingga selesai!
     */
    const PROSES_TYPE_TA = 'menunggu_template_awal';
    const PROSES_TYPE_MP = 'menunggu_pemenuhan';
    const PROSES_TYPE_MS = 'menunggu_supervisi';
    const PROSES_TYPE_FS = 'proses_selesai';

    protected $table = 'draft';
    protected $fillable = ['judul', 'status', 'jenis_aturan'];

    /**
     * Relasi ke file milik Draft!
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileDrafts()
    {
        return $this->hasMany('Eh\FileDraft');
    }

    public function user()
    {
        return $this->belongsTo('Eh\User');
    }

    public function skpd()
    {
        return $this->belongsTo('Eh\SKPD');
    }
}
