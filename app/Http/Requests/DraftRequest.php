<?php

namespace Eh\Http\Requests;

use Eh\Factories\JenisAturanFactory;
use Eh\Http\Requests\Request;

class DraftRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $jenisAturan = implode(',', array_keys(JenisAturanFactory::lists()));
        return [
            'judul' => 'required|max:255',
            'jenis_aturan' => 'required|in:'.$jenisAturan
        ];
    }
}
