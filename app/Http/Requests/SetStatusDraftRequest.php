<?php

namespace Eh\Http\Requests;

use Eh\Factories\DraftFactory;
use Eh\Http\Requests\Request;

class SetStatusDraftRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Authorization::getIsLoggedUserRoleAs('supervisor');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = implode(',', array_keys(DraftFactory::getStatusLists()));
        return [
            'status' => "required|in:".$status
        ];
    }
}
