<?php

namespace Eh\Http\Controllers;

use Eh\Draft;
use Eh\Factories\DraftFactory;
use Eh\FileAttachment;
use Eh\FileDraft;
use Illuminate\Http\Request;

use Eh\Http\Requests;
use Eh\Http\Controllers\Controller;
use Panatau\Authorization\AuthorizationFacade;
use Panatau\Tools\FileAttachmentTrait;

class DraftController extends Controller
{
    use FileAttachmentTrait, KomentarControllerTrait;
    /** @var  DraftFactory */
    protected $factory;
    public function __construct(DraftFactory $draftFactory)
    {
        $this->factory = $draftFactory;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('draft.index')
            ->with('data', $this->factory->getDataIndex())
            ->with('layout', $this->getLayout());
    }

    public function indexDataTable()
    {
        return view('draft.index-datatable')
            ->with('layout', $this->getLayout());
    }

    public function getDataBTTable()
    {
        return $this->factory->getBTTable($this->pagingTableBootstrap());
    }

    /**
     * @param $id
     */
    public function postDeleteDraft($id)
    {
        $draft = $this->factory->getDraft($id);
        $ok = $this->factory->delete($draft);
        if($this->isIntercoolerRequest())
        {
            if($ok)
            {
                return response("Draft Telah Terhapus", 200,
                ['X-IC-Remove'=>true]);
            }
            else
            {
                return response()->json($this->factory->getErrors(), 500, ['X-IC-Remove'=>false]);
            }
        }
    }

    /**
     * Dapatkan form dan hanya bereaksi terhadap intercooler request
     */
    public function getCreateForm()
    {
        if($this->isIntercoolerRequest())
        {
            return view('draft.form')
                ->with('data', null)
                ->with('method', 'put')
                ->with('action', route('draft.store'));
        }
        return 'NOT INTERCOOLER REQ!';
    }

    /**
     * Kembalikan tampilan draft list!
     * @return $this|string
     */
    public function getDraftList()
    {
        if($this->isIntercoolerRequest())
        {
            return view('draft.draftList')
                ->with('data', $this->factory->getDataIndex());
        }
        return 'NOT INTERCOOLER REQ!';
    }

    /**
     * Load form untuk melakukan upload file draft awal.
     * targetOwnerId adalah ID element dari DOM HTML yang menjadi tempat form upload ini, sehingga fungsi yang bisa
     * dipanggil dari event form saat melakukan trigger tidak meracuni global!
     * @param int $id dari draft yang ingin ditambahkan draft awalnya
     */
    public function getUploadDraftAwal($id, $targetOwnerId=null)
    {
        return view('draft.modalUploadDraftAwal')
            ->with('routesUpload', route('draft.postUploadDraftAwal', ['id'=>$id, 'targetOwnerId'=>$targetOwnerId]))
            ->with('targetOwnerId', $targetOwnerId)
            ->with('id', $id);
    }

    /**
     * Yang ini untuk upload draft revisi!
     * @param $id
     * @param null $targetOwnerId
     * @return mixed
     */
    public function getUploadDraftRevisi($id,$targetOwnerId=null)
    {
        return view('draft.modalUploadDraftAwal')
            ->with('routesUpload', route('draft.postUploadDraftRevisi', ['id'=>$id, 'targetOwnerId'=>$targetOwnerId]))
            ->with('targetOwnerId', $targetOwnerId)
            ->with('id', $id);
    }

    /**
     * Proses penyimpanan sebenarnya dilakukan!
     * @param $id
     * @param $targetOwnerId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    protected function realPostUploadDraft($id, $targetOwnerId)
    {
        $rules = [
            'keterangan' => 'required',
            'filea' => 'required'

        ];
        $input = \Input::all();
        $v = \Validator::make($input, $rules);
        if($v->fails())
        {
            return \Response::make("<script type='text/javascript'>parent.$('#{$targetOwnerId}').trigger('validateError', ['Keterangan dan File wajib isi']);</script>");
        }
        // rules sudah oke sekarang saatnya disimpan
        // pastikan integritas terjaga! Jangan simpan data bila tidak bisa diupload.
        // simpan dulu file!
        $response = '';
        try
        {
            \DB::transaction(function() use($id, $input, $targetOwnerId, &$response) {
                $draft = Draft::findOrFail($id);
                $fileDraft = new FileDraft();
                $fileDraft->draft_id = $draft->id;
                $fileDraft->user_id = \Auth::user()->id;
                $fileDraft->keterangan = $input['keterangan'];
                // todo: bagaimana bila yang upload bukan operator dan bukan pula supervisor?
                $uploaderIsOperator = \Authorization::getIsLoggedUserRoleAs('operator');
                $fileDraft->sumber = $uploaderIsOperator?
                    FileDraft::SUMBER_PENGAJU:
                    FileDraft::SUMBER_SUPERVISOR;
                // set status draft
                if($uploaderIsOperator)
                {
                    // bila uploader adalah operator maka tipenya adalah menunggu supervisi
                    $draft->proses_type = Draft::PROSES_TYPE_MS;
                }
                else
                {
                    // bila yang lain maka menunggu revisi
                    $draft->proses_type = Draft::PROSES_TYPE_MP;
                }
                $draft->save(); // simpan untuk update
                $draft->fileDrafts()->save($fileDraft);
                if(!$this->uploadAttachment($fileDraft, 'Eh\FileAttachment', public_path('uploads'), $targetOwnerId, $response))
                {
                    throw new \Exception('Data draft Rollback');
                }
            });
        }
        catch(\Exception $e)
        {
            \Log::warning($e->getMessage()."\n".$e->getTraceAsString());
        }
//        \Log::debug($response, ['targetOwner'=>$targetOwnerId]);
        return response($response);
    }

    /**
     * Upload draft revisi!
     * @param int $id ID Draft yang ingin ditambahkan
     * @param null $targetOwnerId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function postUploadDraftRevisi($id, $targetOwnerId=null)
    {
        return $this->realPostUploadDraft($id, $targetOwnerId);
    }

    /**
     * Simpan hasil upload!
     * Untuk draft awal maka file_draft akan disimpan disini!
     * @param int $id dari draft yang ingin ditambahkan
     */
    public function postUploadDraftAwal($id, $targetOwnerId=null)
    {
        return $this->realPostUploadDraft($id, $targetOwnerId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DraftRequest $request)
    {
        $res = $this->factory->store($request->only(['judul', 'jenis_aturan']));
        if($res && $this->isIntercoolerRequest())
        {
           // kembalikan tampilan untuk tombol dan juga trigger fungsi di index icCreateFormSuccess
           return response(view('draft.buttonAction')->render(), 200,
               ['X-IC-Trigger'=>'draft/icCreateFormSuccess']);
        }
        return response('success');
    }

    /**
     * Tampilkan halaman untuk review proses
     * @param int $id ID Draft yang akan dilihat/review prosesnya
     */
    public function getReview($id)
    {
        $draft = $this->factory->getDraft($id);
        return view('draft.review')
            ->with('layout', $this->getLayout())
            ->with('draft', $draft)
            ->with('filesDraft', $this->factory->getFilesDraft($draft) );
    }

    /**
     * Post komentar di File revisian
     * @param int $id ini adalah ID dari Draft
     * @param int $fileDraftId ini adalah ID dari fileDraft
     */
    public function postKomentarDiFile($fileDraftId)
    {
        // dapatkan fileDraft
        $flDraft = FileDraft::findOrFail($fileDraftId);
        $input = \Input::all();
        if(!$this->checkKomentarValidator($input))
        {
            return response("Nilai komentar harus diisi", 422);
        }
        if($this->postKomentar($flDraft, $input))
        {
            return response(view('komentar.daftar', ['komentar'=>$flDraft->comments])->render());
        }
        return response("Gagal menyimpan?", 501);
    }

    /**
     * Set status draft kita!
     * @param int $id ID Draft
     * @param $status Status draft!
     */
    public function postSetStatusDraft($id, $status)
    {
        $status_array = $this->factory->getStatusLists();
        if(!array_key_exists($status, $status_array))
        {
            return response("Nilai status tidak benar!", 422);
        }

        $draft = Draft::findOrFail($id);
        if($this->factory->setStatusDraft($draft, $status))
        {
            //
            return response("Status berhasil diubah");
        }
        return response('Gagal melakukan setting proses', 500);
    }
}
