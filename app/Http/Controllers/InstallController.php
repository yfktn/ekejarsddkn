<?php

namespace Eh\Http\Controllers;

use Eh\Http\Requests;

class InstallController extends Controller
{
    public function install($p='')
    {
        if($p!="ABCDEF123") {
            \App::abort(401,'NO!');
        }
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
    }
}
