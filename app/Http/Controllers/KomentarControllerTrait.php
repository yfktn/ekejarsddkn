<?php
/**
 * Ii digunakan untuk mempermudah proses dengan komentar!
 * User: toni
 * Date: 17/11/15
 * Time: 22:56
 */

namespace Eh\Http\Controllers;


use Eh\Komentar;
use Illuminate\Database\Eloquent\Model;

Trait KomentarControllerTrait
{

    /**
     * Check validasi komentar
     * @param $input
     * @return bool
     */
    protected function checkKomentarValidator($input)
    {
        $rules = [
            'isi' => 'required'
        ];
        return \Validator::make($input, $rules)->passes();
    }
    /**
     * Masukkan ke data komentar, ingat bahwa modelOwner harus memberikan link ke polymorphic milik komentar
     * @param Model $modelOwner
     */
    protected function postKomentar(Model $modelOwner, $input)
    {
        $k = new Komentar();
        $k->isi = $input['isi'];
        $k->user_id = \Auth::user()->id;
        return $modelOwner->comments()->save($k);
    }
}