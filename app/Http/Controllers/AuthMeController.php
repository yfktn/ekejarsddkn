<?php

namespace Eh\Http\Controllers;

use Eh\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;

use Eh\Http\Requests;

/**
 * Implementasikan untuk melakukan integrasi dengan ic-controller dan juga dengan kemampuan untu login dengan email
 * dan juga dengan userid
 * Class AuthMeController
 * @package Eh\Http\Controllers
 */
class AuthMeController extends AuthController
{
    /**
     * Override dengan cara mengubah request dengna menambahkan field untuk username, disini adalah name ...
     * @param Request $request
     */
    protected function getCredentials(Request $request)
    {
        // bila benar email yang diinputkan maka kita check berdasarkan email, bila tidak maka kita check berdasarkan name
        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        // masukkan field ke request dengan nilai yang diinputkan dari email
        $request->merge([$field=>$request->input('email')]);
        return $request->only($field, 'password');
    }

    /**
     * kembalikan berupa permintaan redirect ...
     * @param Request $request
     * @param $guard
     */
    protected function authenticated(Request $request, $guard)
    {
        return response('', 200, ['X-IC-Redirect'=>'/']);
    }
}
