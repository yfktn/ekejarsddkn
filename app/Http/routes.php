<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', 'AuthMeController@getLogin');
Route::post('auth/login', 'AuthMeController@postLogin');
Route::get('auth/logout', 'AuthMeController@getLogout');
Route::get('auth/register', 'AuthMeController@getRegister');
Route::post('auth/register', 'AuthMeController@postRegister');

Route::group(['middleware'=>'auth'], function() {
    Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

//    Route::get('draft', ['as' => 'draft', 'uses' => 'DraftController@index']);
    Route::get('draft', ['as' => 'draft', 'uses' => 'DraftController@indexDataTable']);
    Route::get('draft/getDataBTTable', ['as' => 'draft.getDataBTTable', 'uses' => 'DraftController@getDataBTTable']);
    Route::delete('draft/delete/{id}', ['as' => 'draft.delete', 'uses' => 'DraftController@postDeleteDraft']);
    Route::get('draft/getDraftList', ['as' => 'draft.getDraftList', 'uses' => 'DraftController@getDraftList']);
    Route::get('draft/getUploadDraftAwal/{id}/{targetOwnerId?}', ['as' => 'draft.getUploadDraftAwal', 'uses' => 'DraftController@getUploadDraftAwal']);
    Route::get('draft/getUploadDraftRevisi/{id}/{targetOwnerId?}', ['as' => 'draft.getUploadDraftRevisi', 'uses' => 'DraftController@getUploadDraftRevisi']);
    Route::post('draft/postUploadDraftAwal/{id}/{targetOwnerId?}', ['as' => 'draft.postUploadDraftAwal', 'uses' => 'DraftController@postUploadDraftAwal']);
    Route::post('draft/postUploadDraftRevisi/{id}/{targetOwnerId?}', ['as' => 'draft.postUploadDraftRevisi', 'uses' => 'DraftController@postUploadDraftRevisi']);
    Route::get('draft/getCreateForm', ['as' => 'draft.getCreateForm', 'uses' => 'DraftController@getCreateForm']);
    Route::get('draft/getReview/{id}', ['as' => 'draft.getReview', 'uses' => 'DraftController@getReview']);
    Route::put('draft/store', ['as' => 'draft.store', 'uses' => 'DraftController@store']);
    Route::post('draft/postKomentarDiFile/{fileDraftId}', ['as' => 'draft.postKomentarDiFile', 'uses' => 'DraftController@postKomentarDiFile']);
    Route::post('draft/postSetStatusDraft/{id}/{status}', ['as' => 'draft.postSetStatusDraft', 'uses' => 'DraftController@postSetStatusDraft']);

    // ------------------------------
    // MENGINGAT
    // ------------------------------
    Route::get('mengingat', ['as' => 'mengingat', 'uses' => 'ToolMengingatController@index']);
    Route::post('postPencarian', ['as' => 'mengingat.postPencarian', 'uses' => 'ToolMengingatController@postPencarian']);

});