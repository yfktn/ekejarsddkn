<?php

use Illuminate\Database\Seeder;

class InitSKPDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $n = \Eh\SKPD::firstOrNew(['nama'=>'Biro Hukum SETDA Provinsi Kalimantan Tengah']);
        $n->save();
        $n = \Eh\SKPD::firstOrNew(['nama'=>'Biro Ekonomi SETDA Provinsi Kalimantan Tengah']);
        $n->save();
        $n = \Eh\SKPD::firstOrNew(['nama'=>'Biro Keuangan SETDA Provinsi Kalimantan Tengah']);
        $n->save();
    }
}
