<?php

use Illuminate\Database\Seeder;

class InitAuthSeeder extends Seeder
{
    /**
     * Init authorization
     *
     * @return void
     */
    public function run()
    {
        $auth = app('Panatau\Authorization\AuthorizationInterface');
        // role
        $auth->createRole('admin');
        $auth->createRole('operator');
        $auth->createRole('supervisor');
        // set ke User
        /** @var \Eh\User $user */
        $user = \Eh\User::where('name', '=', 'admin')->first();
        $user->addRole('admin');
        $user = \Eh\User::where('name', '=', 'op-keu')->first();
        $user->addRole('operator');
        $user = \Eh\User::where('name', '=', 'op-eko')->first();
        $user->addRole('operator');
        $user = \Eh\User::where('name', '=', 'sp')->first();
        $user->addRole('supervisor');
    }
}
