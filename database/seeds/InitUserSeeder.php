<?php
use Illuminate\Database\Seeder;

class InitUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * PASTIKAN bahwa InitSKPDSeeder sudah dilakukan lebih dahulu!
     *
     * @return void
     */
    public function run()
    {
        // Admin dan supervisor tidak perlu ada skpd
        $users = \Eh\User::firstOrNew(['name'=>'admin']);
        $users->email = 'admin@eh.com';
        $users->password = bcrypt('rahasia');
        $users->save();

        $users = \Eh\User::firstOrNew(['name'=>'sp']);
        $users->email = 'sp@eh.com';
        $users->password = bcrypt('rahasia');
        $users->save();

        // anggap ada operator dari Biro Ekonomi!
        $skpd = \Eh\SKPD::where('nama', '=', 'Biro Ekonomi SETDA Provinsi Kalimantan Tengah')->first();
        $users = \Eh\User::firstOrNew(['name'=>'op-eko']);
        $users->email = 'op-eko@eh.com';
        $users->password = bcrypt('rahasia');
        $users->skpd_id = $skpd->id;
        $users->save();

        // anggap ada operator dari Biro Ekonomi!
        $skpd = \Eh\SKPD::where('nama', '=', 'Biro Keuangan SETDA Provinsi Kalimantan Tengah')->first();
        $users = \Eh\User::firstOrNew(['name'=>'op-keu']);
        $users->email = 'op-keu@eh.com';
        $users->password = bcrypt('rahasia');
        $users->skpd_id = $skpd->id;
        $users->save();
    }
}
