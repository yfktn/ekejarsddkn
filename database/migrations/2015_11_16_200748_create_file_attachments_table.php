<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_attachment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_file_name', 256)->nullable();
            $table->string('file_name')->default('');
            $table->string('folder')->default('');
            $table->string('slug')->nullable();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->string('owner_type', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_attachment');
    }
}
