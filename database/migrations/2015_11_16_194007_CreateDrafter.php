<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrafter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft', function(Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->unsignedInteger('user_id');
            // proses | final | dibatalkan
            $table->string('status', 15)->index();
            $table->string('jenis_aturan',30)->nullable()->index();
            $table->timestamps();
        });

        Schema::create('file_draft', function(Blueprint $table) {
            $table->increments('id');
            $table->text('keterangan');
            $table->unsignedInteger('draft_id');
            // pengaju | supervisor
            $table->string('sumber', 15)->index();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_draft');
        Schema::drop('draft');
    }
}
